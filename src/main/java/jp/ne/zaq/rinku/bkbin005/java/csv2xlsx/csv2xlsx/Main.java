/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.java.csv2xlsx.csv2xlsx;

import java.io.IOException;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Convert CSV files to a xlsx file.
 * @author Mitsutoshi NAKANO bkbin005@rinku.zaq.ne.jp
 */
public class Main {
  private static final String FROM_CODE = "from-code";
  private static final String OUTPUT = "output";
  private static final String HELP = "help";
  private static final String OUTPUT_EXCEL = OUTPUT + ".xlsx";
  private static final String USAGE = "[--" + FROM_CODE + " encode] [--"
  + OUTPUT + " xlsxFile] csvFiles... | --" + HELP;

  /** instance.  */
  private static Csv2xlsx instance = new Csv2xlsx();

  /**
   * Main method.
   * @param args
   * @throws IOException could not open files.
   * @throws ParseException
   */
  public static void main(String[] args) throws IOException, ParseException {
    Options options = createOptions();
    CommandLine cmd;
    try {
      cmd = parseOptions(options, args);
    } catch (ParseException e) {
      System.err.println(USAGE);
      throw e;
    }
    if (cmd.hasOption(HELP)) {
      showusage(options);
      return;
    }
    String inputEncoding = cmd.getOptionValue(FROM_CODE);
    String xlsxfile = cmd.getOptionValue(OUTPUT);
    if (xlsxfile == null) {
      xlsxfile = OUTPUT_EXCEL;
    }

    List <String> csvfiles = cmd.getArgList();
    instance.createExcelfile(xlsxfile, csvfiles, inputEncoding);
  }

  private static void showusage(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp(USAGE, options);
  }

  /**
   * Parese options.
   * @param options options object.
   * @param args arguments.
   * @return CommandLine object.
   * @throws ParseException Parse error.
   */
  static CommandLine parseOptions(Options options, String[] args)
      throws ParseException {
    return (new DefaultParser()).parse(options, args);
  }

  /**
   * Create options.
   * @return Options object.
   */
  static Options createOptions() {
    Options options = new Options();
    options.addOption(new Option("?", HELP, false, "show help"));
    options.addOption(new Option("f", FROM_CODE, true, "input encoding"));
    Option output = new Option("o", OUTPUT, true, "output file");
    options.addOption(output);
    return options;
  }

}
