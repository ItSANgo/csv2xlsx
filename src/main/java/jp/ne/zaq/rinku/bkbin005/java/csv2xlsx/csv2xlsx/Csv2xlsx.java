/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.java.csv2xlsx.csv2xlsx;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.orangesignal.csv.Csv;
import com.orangesignal.csv.CsvConfig;
import com.orangesignal.csv.handlers.StringArrayListHandler;

/**
 * Convert CSV files to a xlsx file.
 * @author Mitsutoshi NAKANO bkbin005@rinku.zaq.ne.jp
 */
public class Csv2xlsx {
  private static final String DEFAULT_ENCODING = System
      .getProperty("file.encoding");

  /**
   * Create Excel file.
   * @param xlsxfile
   * @param csvfiles
   * @param inputEncoding
   * @throws IOException
   */
  public void createExcelfile(String xlsxfile, List<String> csvfiles,
      String enc) throws IOException {
    String inputEnc = getInputEncoding(enc);
    Workbook wb = createWorkbook();
    for (String csvfilename : csvfiles) {
      insertSheet(wb, csvfilename, inputEnc);
    }
    try (OutputStream fileOut = new FileOutputStream(xlsxfile)) {
      wb.write(fileOut);
    }
  }

  /**
   * Get input Encoding.
   * @param enc encoding from option.
   * @return encoding name.
   */
  String getInputEncoding(String enc) {
    return (enc != null) ? enc : DEFAULT_ENCODING;
  }

  /**
   * Insert a sheet to the workbook.
   * @param wb workbook
   * @param csvfilename CSV file
   * @param inputEnc
   * @throws IOException could not load.
   */
  void insertSheet(Workbook wb, String csvfilename, String inputEnc)
      throws IOException {
    String sheetname = getSheetname(csvfilename);
    List<String[]> list = load(csvfilename, inputEnc);
    Sheet sheet = createSheet(wb, sheetname);
    setValues(sheet, list);
  }

  /**
   * Get sheet name.
   * @param csvfilename CSV file path.
   * @return sheet name.
   */
  String getSheetname(String csvfilename) {
    String name = (new File(csvfilename)).getName();
    return name.replaceAll("\\.csv$", "");
  }

  /**
   * Create new workbook.
   * @return workbook.
   */
  Workbook createWorkbook() {
    return new XSSFWorkbook();
  }

  /**
   * Load CSV file.
   * @param fname CSV filename.
   * @param inputEnc
   * @return CSV data.
   * @throws IOException could not load.
   */
  List<String[]> load(String fname, String inputEnc) throws IOException {
    return Csv.load(new File(fname), inputEnc,  new CsvConfig(),
        new StringArrayListHandler());
  }

  /**
   * Create a sheet.
   * @param wb target workbook.
   * @param sheetname sheet name.
   * @return new sheet.
   */
  Sheet createSheet(Workbook wb, String sheetname) {
    return wb.createSheet(sheetname);
  }

  /**
   * Set values to a sheet.
   * @param sheet sheet
   * @param list CSV file information.
   */
  void setValues(Sheet sheet, List <String[]> list) {
    for (int i = 0; i < list.size(); ++i) {
      Row row = sheet.createRow(i);
      setRow(row, list.get(i));
    }
  }

  /**
   * Set a row.
   * @param row Excel row.
   * @param line CSV row.
   */
  void setRow(Row row, String[] line) {
    for (int j = 0; j < line.length; ++j) {
      Cell cell = row.createCell(j);
      cell.setCellValue(line[j]);
    }
  }
}
